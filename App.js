/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { View, Text, Image, Dimensions, StyleSheet, StatusBar, ScrollView } from 'react-native';
import OvoComponent from './source/component/OvoComponent';
import MainComponent from './source/component/MainComponent';
import PromoItems from './source/component/PromoItems';

import Routes from './source/config/routes'

const { height, width } = Dimensions.get('window')

class Home extends React.Component {
  render() {
    return (
      <Routes />
    )
  }
}

const styles = StyleSheet.create({
  imageBanner: {
    height: 140, width: width,
  },
  greetingText: {
    fontSize: 17,
    fontWeight: 'bold',
    position: 'absolute',
    alignSelf: 'center',
    top: 30,
    color: '#383838'
  },
  wrapperOvo: {
    marginHorizontal: 18,
    height: 150,
    marginTop: -60,
    backgroundColor: 'white',
    elevation: 4,
    borderRadius: 10,
  },
  textOVO: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 12,
    marginTop: 10,
  },
  garisDiOvo: {
    height: .8,
    backgroundColor: '#adadad',
    marginTop: 10,
    marginHorizontal: 5,
  },
  divider: {
    width: width,
    height: 15,
    backgroundColor: '#ededed',
    marginVertical: 15
  }
})
export default Home