import React, {Component} from 'react'
import {View, Text, Image, ScrollView, StyleSheet} from 'react-native'

const images = [
	'https://images-loyalty.ovo.id/public/deal/54/09/l/21178.jpg?ver=1',
	'https://images-loyalty.ovo.id/public/deal/05/65/l/17532.jpg?ver=1',
	'https://images-loyalty.ovo.id/public/deal/63/94/l/19670.jpg?ver=1',
	'https://images-loyalty.ovo.id/public/deal/77/72/l/24142.jpg?ver=1',
	'https://images-loyalty.ovo.id/public/deal/78/88/l/15438.jpg?ver=1',

]

export default class InfoPromo extends React.Component {

	state = {
		active: 0
	}

	change = ({nativeEvent}) => {
		const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
		if(slide !== this.state.active){
			this.setState({active: slide});
		}
	}
	render() {
		return(
			<View style={style.container}>
			<ScrollView 
				pagingEnabled 
				horizontal
				onScroll={this.change}
				showsHorizontalScrollIndicator={false}>
			{
				images.map((image, index) => (
					<Image
					key={index}
					source={{uri: image}} 
					style={style.image} />
					))

			}
			</ScrollView>
			<View style={style.text}>
			{
				images.map((i,k) => (
					<Text key={k} style={k==this.state.active ? style.fontActive : style.font}>⚊</Text>
				))
			}
			</View>
			</View>
	



			)
	}
}	

const style = StyleSheet.create({
	container: {marginTop: 15, marginLeft: 5,},
	image: {width: 340, height: 140, borderRadius: 8, marginLeft:12},
	text: {flexDirection:'row', position:'absolute', bottom:-25, alignSelf:'center'},
	font: {fontSize: 25, color:'lightgrey'},
	fontActive: {fontSize: 25, color:'#00e1ed'}

})
