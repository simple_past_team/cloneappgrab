import React, { Component } from 'react'
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native'
import { useNavigation } from '@react-navigation/native'



const styles = StyleSheet.create({
  wrapperPay: {
    flexDirection: 'row',
    justifyContent:'space-between'
  },
  ovoFeatureImage: {
    height: 30,
    width: 30,
    marginTop: 17,
    marginHorizontal: 39,
  }
})
const OvoComponent = () => {
  const navigation = useNavigation();
	return (
		<View style={styles.wrapperPay}>
            <TouchableOpacity onPress={() => navigation.navigate('topup')}>
              <Image style={styles.ovoFeatureImage} source={require('../assets/Icon/topup.png')} />
              <Text style={{alignSelf: 'center', marginTop: 5, fontSize: 12,color: '#671178'}}>Top Up</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Tranfer')}>
              <Image style={styles.ovoFeatureImage} source={require('../assets/Icon/trans.png')} />
              <Text style={{alignSelf: 'center', marginTop: 5, fontSize: 12,color: '#671178'}}>Transfer</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('History')}>
              <Image style={styles.ovoFeatureImage} source={require('../assets/Icon/history.png')} />
              <Text style={{alignSelf: 'center', marginTop: 5, fontSize: 12,color: '#671178'}}>History</Text>
            </TouchableOpacity>
         </View>
		)
}
export default OvoComponent