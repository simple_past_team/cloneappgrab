import React, {Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import PromoItemsSub from './PromoItemsSub'

const PromoItems = () => {
	return (
		<View style={{marginTop: 15,marginHorizontal: 18, width: '100%', flexWrap: 'wrap',flexDirection:'row'}}>
			<PromoItemsSub 
				image={{uri : "https://3.bp.blogspot.com/_g3zfeo7CWiY/TSCMfmFnevI/AAAAAAAAAFM/Ll7envOnAR4/s1600/question.jpg"}}
				text="Pusat Bantuan"
				info="Punya kendala atau pertanyaan terkait OVO? Kamu bisa kirim di sini"
				button="Lihat Bantuan"
			/>

			<PromoItemsSub 
				image={{uri : "https://images.bisnis-cdn.com/posts/2020/05/17/1241675/ovo-prudential-gelar-assuransi-gratis-corona.jpg"}}
				text="Perlindungan COVID-19"
				info="Dapatkan perlindungan COVID-19 bebas biaya"
				button="Daftar Sekarang"
				
			/>

			
		</View>
		)
}

export default PromoItems