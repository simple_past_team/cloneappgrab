import React from 'react'
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native'



const styles = StyleSheet.create({

  imageMain: {
    height: 40,
    width: 40,
    marginTop: 15,
    borderRadius: 20
  },
  textMain: {
    textAlign: 'center',
    marginTop: 5,
    fontSize: 12
  }
})

class MainComponent2 extends React.Component{
  render() {
    return (
    
    <TouchableOpacity onPress={this.props.onPress} style={{width: '25%', alignItems: 'center'}}>
      <Image style={styles.imageMain} source={this.props.image} />
      <Text style={styles.textMain}>{this.props.title}</Text>
    </TouchableOpacity>
      )
    }
  }


export default MainComponent2