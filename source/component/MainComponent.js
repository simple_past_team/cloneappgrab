import React from 'react'
import {Image, Text, View, StyleSheet} from 'react-native'
import MainComponent2 from './MainComponent2'
import { useNavigation } from '@react-navigation/native'
import { TouchableOpacity } from 'react-native-gesture-handler';


const styles = StyleSheet.create({
  wrapperMain: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    flexWrap: 'wrap',
    width: '100%'
  },
})
const MainComponent = () => {
  const navigation = useNavigation();
	return (
		<View style={styles.wrapperMain}>
    <MainComponent2 onPress={() => navigation.navigate('PLN')} image={require('../assets/Icon/5.png')} title="PLN" />
    <MainComponent2 onPress={() => navigation.navigate('Pulsa')} image={require('../assets/Icon/2.png')} title="Pulsa" />
    <MainComponent2 onPress={() => navigation.navigate('paketData')} image={require('../assets/Icon/3.png')} title="Paket Data" />
    <MainComponent2 onPress={() => navigation.navigate('PDAM')} image={require('../assets/Icon/6.png')} title="Air PDAM" />
    <MainComponent2 onPress={() => navigation.navigate('BPJS')} image={require('../assets/Icon/4.png')} title="BPJS" />
    <MainComponent2 onPress={() => navigation.navigate('TV')} image={require('../assets/Icon/1.png')} title="Internet & TV Kabel" />
    <MainComponent2 onPress={() => navigation.navigate('telkom')} image={require('../assets/Icon/8.png')} title="Telkom" />
    <MainComponent2 image={require('../assets/Icon/7.png')} title="Lainnya" />
    </View>
		)
}
export default MainComponent