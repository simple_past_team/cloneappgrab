import React, { Component } from 'react'
import {View, Text, StyleSheet, Dimensions, Image} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

const { width } = Dimensions.get('window')
const styles= StyleSheet.create({
	wrapper: {
		backgroundColor: 'white',
		elevation: 4,
		borderRadius: 8,
		width:width /2 -27,
		marginRight: 18,
		marginBottom: 18,
	},
	PromoImage: {
		height: width/3-27,
		width : width / 2 - 27,
		borderTopLeftRadius: 10,
		borderTopRightRadius: 10
	},
	TextPromo: {
		marginLeft:10,
		marginVertical:10,
	},
	
	button: {
		position:'absolute', alignSelf:'center', fontSize:13, color:'#00e1ed'
	}
})
const PromoItemsSub = (props) => {
	return (
		<View style={styles.wrapper}>

				<Image source={props.image} style={styles.PromoImage} />
				<View style={styles.TextPromo}>
				<Text style={{fontWeight:'bold', fontSize:15}}>{props.text}</Text>
				</View>
				<View style={{marginLeft:10, marginBottom:8, flexDirection: 'row'}}>
        		<Text style={{marginLeft:0, fontSize:13, color:'#575757'}}>{props.info}</Text>
        		</View>
        		<View style={{ marginBottom:20, marginTop:25, flexDirection: 'row', marginLeft: 30}}>
        		<Text style={styles.button}>{props.button}</Text>
      			</View>
		</View>
		)
}

export default PromoItemsSub