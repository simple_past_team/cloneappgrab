import React, { Component } from 'react'
import { Text, View, Image, ColorPropType } from 'react-native'

import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator, HeaderBackground } from '@react-navigation/stack'

import { Home, Deals, Scan, finance, profile, PLN, topup, Tranfer, History, Pulsa, paketData, PDAM, BPJS, TV, telkom } from '../../page'
import { color } from 'react-native-reanimated'
import { Colors } from 'react-native/Libraries/NewAppScreen'

const MaterialBottom = createBottomTabNavigator();
const Stack = createStackNavigator();

const HomeStack = () => {
	return (
		<Stack.Navigator
		
		>
			<Stack.Screen name="OVO" component={BottomTabs} 
			options={{
				title:"OVO",
				headerShown:false
			}}
			/>
			<Stack.Screen name="topup" component={topup} 
			options={{
				title:"Top Up",
				headerShown:true,
				headerTransparent:true,
				headerTintColor: "white"
			}}
			/>
			<Stack.Screen name="Tranfer" component={Tranfer} 
			options={{
				title:"TRANSFER",
				headerShown:true,
				headerTransparent:true,
				headerTintColor: "white"
			}}
			/>
			<Stack.Screen name="History" component={History} 
			options={{
				title:"History",
				headerShown:true,
				headerTransparent:true,
				headerTintColor: "white"
			}}
			/>
			<Stack.Screen name="PLN" component={PLN} />
			<Stack.Screen name="Pulsa" component={Pulsa} 
			options={{
				title:"Pulsa & Paket Data",
				headerShown:true,
				headerTransparent:true,
				headerTintColor: "black"
			}}
			/>
			<Stack.Screen name="paketData" component={paketData} 
			options={{
				title:"Pulsa & Paket Data",
				headerShown:true,
				headerTransparent:true,
				headerTintColor: "black"
			}}
			/>
			<Stack.Screen name="PDAM" component={PDAM} 
			options={{
				title:"Air PDAM",
				headerShown:true,
				headerTransparent:true,
				headerTintColor: "black"
			}}
			/>
			<Stack.Screen name="BPJS" component={BPJS} 
			options={{
				title:"BPJS",
				headerShown:true,
				headerTransparent:true,
				headerTintColor: "black"
			}}
			/>
			<Stack.Screen name="TV" component={TV} 
			options={{
				title:"Internet & TV Kabel",
				headerShown:true,
				headerTransparent:false,
				headerTintColor: "black"
			}}
			/>
			<Stack.Screen name="telkom" component={telkom} 
			options={{
				title:"Telkom",
				headerShown:true,
				headerTransparent:true,
				headerTintColor: "black"
			}}
			/>

		</Stack.Navigator>
	)
}

const BottomTabs = () => {
	return (
		<MaterialBottom.Navigator tabBarOptions={{
			shifting: 'false',
			initialRouteName: 'Home',
			activeTintColor: '#5703ab',
			style: { height: 50, borderWidth: .3, borderColor: 'lightgrey' },
		}}>
			<MaterialBottom.Screen name="Home" component={Home}
				options={{
					tabBarLabel: 'Home',
					tabBarIcon: ({ color }) => (
						<View style={{ marginTop: -4 }}>
							{color == "#5703ab" ?
								<Image source={require('../../assets/Icon/navi1-active.png')} style={{ height: 30, width: 30 }} />
								:
								<Image source={require('../../assets/Icon/navi1.png')} style={{ height: 30, width: 30 }} />
							}
						</View>
					)
				}}
			/>
			<MaterialBottom.Screen name="Deals" component={Deals}
				options={{
					tabBarIcon: ({ color }) => (
						<View style={{ marginTop: -4 }}>
							{color == "#5703ab" ?
								<Image source={require('../../assets/Icon/navi2-active.png')} style={{ height: 30, width: 30 }} />
								:
								<Image source={require('../../assets/Icon/navi2.png')} style={{ height: 30, width: 30 }} />
							}
						</View>
					)
				}}
			/>
			<MaterialBottom.Screen name="Scan" component={Scan}
				options={{
					tabBarIcon: ({ color }) => (
						<View style={{ marginTop: -20, }}>
							{color == "#5703ab" ?
								<Image source={require('../../assets/Icon/scan.png')} style={{ height: 50, width: 50, borderRadius: 25 }} />
								:
								<Image source={require('../../assets/Icon/scan.png')} style={{ height: 50, width: 50, borderRadius: 25, borderWidth: 2, borderColor: 'white' }} />
							}
						</View>
					)
				}}
			/>
			<MaterialBottom.Screen name="Finance" component={finance}
				options={{
					tabBarIcon: ({ color }) => (
						<View style={{ marginTop: -4 }}>
							{color == "#5703ab" ?
								<Image source={require('../../assets/Icon/navi3-active.png')} style={{ height: 30, width: 30 }} />
								:
								<Image source={require('../../assets/Icon/navi3.png')} style={{ height: 30, width: 30 }} />
							}
						</View>
					)
				}}
			/>
			<MaterialBottom.Screen name="Profile" component={profile}
				options={{
					tabBarIcon: ({ color }) => (
						<View style={{ marginTop: -4 }}>
							{color == "#5703ab" ?
								<Image source={require('../../assets/Icon/navi4-active.png')} style={{ height: 30, width: 30 }} />
								:
								<Image source={require('../../assets/Icon/navi4.png')} style={{ height: 30, width: 30 }} />
							}
						</View>
					)
				}}
			/>
		</MaterialBottom.Navigator>
	)
}

export default class index extends Component {
	render() {
		return (
			<NavigationContainer>
				<HomeStack />
			</NavigationContainer>
		)
	}
}