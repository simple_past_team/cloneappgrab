import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, Dimensions, ScrollView, StatusBar } from 'react-native'

const { width } = Dimensions.get('window')
export default class index extends Component {
	render() {
		return (
			<View style={{ flex: 1 }}>
				<View>
					<StatusBar barStyle='dark-content' backgroundColor='white' />
					<Image style={styles.imageBanner} source={require('../../assets/Image/subhome.png')} />
					<View style={{ flexDirection: 'row', flexWrap: 'wrap', position: 'absolute', marginTop: 80, marginLeft: 20 }}>
						<Image style={{ width: 40, height: 40, backgroundColor: 'white', borderRadius: 20, marginTop: -5 }} source={require('../../assets/Icon/indihome.png')} />
						<Text style={{ fontSize: 15, fontWeight: 'bold', marginLeft: 30, marginTop: 5 }}>Telkom IndiHome</Text>
					</View>
				</View>
				<ScrollView>
					<View style={{ flex: 1 }}>
						<StatusBar barStyle='dark-content' backgroundColor='white' />
						<View style={{backgroundColor: 'white', height: 120, elevation: 5}}>
							<Text style={styles.content}>Nomor Pelanggan</Text>
						<View style={styles.wrapper}>
							<Text style={{ marginLeft: 10, marginTop: 15, fontSize: 13, color: '#c9c9c9', marginBottom: 15 }}>Contoh 1234567890</Text>
						</View>
						</View>
					</View>
				</ScrollView>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70, justifyContent: 'space-between' }}>
						<View style={styles.wrapperFooter}>
							<Text style={styles.textFooter}>Lanjut ke Pembayaran</Text>
						</View>
					</View>
			</View>
		)
	}
}
const styles = StyleSheet.create({
	imageBanner: {
		height: 120, width: 400,
	},
	header: {
		fontSize: 15,
		marginHorizontal: 30,
		color: '#4b239b',
		fontWeight: 'bold',
		top: 40
	},
	headerSub: {
		fontSize: 15,
		marginHorizontal: 30,
		color: '#949494',
		fontWeight: 'bold',
		top: 40
	},
	headerDivider: {
		height: 2,
		width: 175,
		backgroundColor: '#4b239b',
		marginVertical: 0,

	},
	content: {
		fontSize: 13,
		marginHorizontal: 14,
		color: '#949494',
		top: 20,
		backgroundColor: 'white'
	},
	content2: {
		fontSize: 15,
		marginHorizontal: 13,
		color: 'black',
		fontWeight: 'bold',
		top: 10
	},
	wrapper: {
		backgroundColor: '#e7dded',
		elevation: 4,
		borderRadius: 10,
		width: width / -10 - 30,
		marginRight: 20,
		marginLeft: 20,
		marginBottom: 10,
		marginTop: 23,
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'space-between'
	},
	footer: {
		fontSize: 15,
		marginHorizontal: 30,
		color: 'white',
		fontWeight: 'bold',
		top: 65
	},
	wrapperFooter: {
		backgroundColor: 'white',
		elevation: 3,
		borderRadius: 20,
		backgroundColor: '#c9c9c9',
		width: 320,
		height: 45,
		marginRight: 10,
		marginLeft: 20,
		marginBottom: 20,
		marginTop: 13,
	},
	textFooter: {
		fontSize: 15,
		marginHorizontal: 85,
		color: 'white',
		fontWeight: 'bold',
		top: 13
	},

})