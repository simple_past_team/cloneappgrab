import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, Dimensions, ScrollView, StatusBar } from 'react-native'

const { width } = Dimensions.get('window')
export default class index extends Component {
	render() {
		return (
			<View style={{ flex: 1 }}>
				<View style={{ flexDirection: 'row', backgroundColor: '#4b239b', height: 100, justifyContent: 'space-between' }}>
					<Text style={styles.header}>Instan Top Up</Text>
					<Text style={styles.headerSub}>Metode Lain</Text>
				</View>
				<View style={styles.headerDivider}></View>
				<ScrollView>
					<View style={{ flex: 1 }}>
					<StatusBar barStyle='dark-content' backgroundColor='white' />
					<View>
						<Text style={styles.content}>Top Up ke</Text>
					</View>
					<View style={styles.wrapper}>
						<View style={{marginBottom: 20}}>
						<Image style={styles.image} source={require('../../assets/Icon/logoOVO.jpeg')} />
						</View>
						<Text style={{ marginTop: 15, marginLeft:40, fontSize: 15, color:'black', fontWeight: 'bold',}}>OVO Cash</Text>
						<Text style={{marginLeft:96, marginTop: -5, fontSize: 12, color:'black', marginBottom:20}}>Balance Rp0</Text>
					</View>
					<View style={styles.divider}></View>
					<View style={{marginBottom:5}}>
						<Text style={styles.content2}>Pilih Nominal Top Up</Text>
					</View>
					<View style={{flexWrap: 'wrap', flexDirection: 'row', marginLeft: 10, marginTop: 0}}>
						<View style={styles.wrapperNominal}>
							<Text style={styles.nominal}>Rp100.000</Text>
						</View>
						<View style={styles.wrapperNominal}>
							<Text style={styles.nominal}>Rp200.000</Text>
						</View>
						<View style={styles.wrapperNominal}>
							<Text style={styles.nominal}>Rp500.000</Text>
						</View>
					</View>
					<View style={{marginBottom:15}}>
						<Text style={styles.content3}>Atau masukan nominal top up di sini</Text>
					</View>
					<View style={{flexWrap: 'wrap', flexDirection: 'row', marginLeft: 10, marginTop: 0}}>
						<View style={styles.wrapperNominal2}>
							<Text style={styles.nominal2}>Minimal Rp10.000</Text>
						</View>
					</View>
					<View style={styles.divider2}></View>
					<View style={{marginBottom:20, marginTop: -10}}>
						<Text style={styles.content2}>Kartu Debit</Text>
					</View>
					<View style={{flexWrap: 'wrap', flexDirection: 'column', marginLeft: 10, marginTop: 0}}>
						<View style={styles.wrapperkartu}>
							<Image style={styles.image1} source={require('../../assets/Icon/atm.png')} />
							<Text style={styles.nominalkartu}>Tambahkan Kartu Debit BCA</Text>
						</View>
					</View>
					<View>
						<Text style={{alignSelf: 'center', marginTop: -15, color: '#02bfbc', marginBottom: 50}}>⬤</Text>
					</View>
					</View>
				</ScrollView>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70, justifyContent: 'space-between' }}>
				<View style={styles.wrapperFooter}>
							<Text style={styles.textFooter}>Top Up Sekarang</Text>
				</View>
				</View>
			</View>
		)
	}
}
const styles = StyleSheet.create({
	header: {
		fontSize: 15,
		marginHorizontal: 30,
		color: 'white',
		fontWeight: 'bold',
		top: 65
	},
	footer: {
		fontSize: 15,
		marginHorizontal: 30,
		color: 'white',
		fontWeight: 'bold',
		top: 65
	},
	wrapperFooter: {
		backgroundColor: 'white',
		elevation: 3,
		borderRadius: 5,
		backgroundColor: '#e8e8e8',
		width: 320,
		height: 45,
		marginRight: 10,
		marginLeft: 20,
		marginBottom: 20,
		marginTop: 13,
	},
	textFooter: {
		fontSize: 15,
		marginHorizontal: 100,
		color: 'white',
		fontWeight: 'bold',
		top: 13
	},
	headerSub: {
		fontSize: 15,
		marginHorizontal: 30,
		color: '#b097c9',
		fontWeight: 'bold',
		top: 65
	},
	headerDivider: {
		height: 3,
		width: 190,
		backgroundColor: '#02bfbc',
		marginVertical: 0,

	},
	content: {
		fontSize: 15,
		marginHorizontal: 13,
		color: 'black',
		fontWeight: 'bold',
		top: 20
	},
	content2: {
		fontSize: 15,
		marginHorizontal: 13,
		color: 'black',
		fontWeight: 'bold',
		top: 10
	},
	wrapper: {
		backgroundColor: 'white',
		elevation: 4,
		borderRadius: 4,
		width: width / -10 - 30,
		marginRight: 20,
		marginLeft: 20,
		marginBottom: 18,
		marginTop: 30,
		flexDirection: 'row',
		flexWrap: 'wrap'
	},
	image: {
		width: 35,
		height: 18,
		borderRadius: 4,
		top: 25,
		marginHorizontal: 10,
	},
	image1: {
		width: 50,
		height: 50,
		borderRadius: 0,
		top: 30,
		marginHorizontal: 90,
	},

	divider: {
		width: width,
		height: 7,
		backgroundColor: '#ededed',
		marginVertical: 2
	},
	divider2: {
		width: width,
		height: 7,
		backgroundColor: '#ededed',
		marginVertical: 30
	},
	wrapperNominal: {
		backgroundColor: 'white',
		elevation: 5,
		borderRadius: 20,
		borderColor: '#e8e8e8',
		borderWidth: 1.5,
		width: 100,
		height: 45,
		marginRight: 10,
		marginLeft: 5,
		marginBottom: 5,
		marginTop: 20,
	},
	nominal: {
		fontSize: 12,
		marginHorizontal: 19,
		color: 'black',
		top: 10
	},
	content3: {
		fontSize: 12,
		marginHorizontal: 13,
		color: '#9e9999',
		top: 10,
	},
	wrapperNominal2: {
		backgroundColor: 'white',
		elevation: 3,
		borderRadius: 5,
		backgroundColor: '#e8e8e8',
		width: 330,
		height: 45,
		marginRight: 10,
		marginLeft: 5,
		marginBottom: 5,
		marginTop: 0,
	},
	nominal2: {
		fontSize: 13,
		marginHorizontal: 20,
		color: '#9e9999',
		top: 13
	},
	wrapperkartu: {
		backgroundColor: 'white',
		elevation: 3,
		borderRadius: 5,
		backgroundColor: '#e8e8e8',
		borderWidth: 1,
		borderColor: '#02bfbc',
		width: 220,
		height: 130,
		marginRight: 10,
		marginLeft: 5,
		marginBottom: 20,
		marginTop: 2,
	},
	nominalkartu: {
		fontSize: 14,
		marginHorizontal: 20,
		color: '#9e9999',
		top: 25
	}

})