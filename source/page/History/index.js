import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, Dimensions, ScrollView, StatusBar } from 'react-native'

const { width } = Dimensions.get('window')
export default class index extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flexDirection: 'row', backgroundColor: '#4b239b', height: 60, justifyContent: 'space-between' }}>
                </View>
                <View style={{ flexDirection: 'column', flexWrap: 'wrap', alignSelf: 'center', marginTop: 80 }}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#4b239b', alignSelf: 'center' }}>View History</Text>
                    <Text style={{ fontSize: 15, color: 'black', alignSelf: 'center' }}>Belum ada transaksi.</Text>
                    <Image style={styles.image} source={require('../../assets/Icon/historyMain.png')} />
                </View>
                <View>
                    <Image style={styles.image1} source={require('../../assets/Icon/doubleArrow.png')} />
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({


    image: {
        width: 80,
        height: 80,
        borderRadius: 4,
        marginTop: 20,
        marginHorizontal: 10,
        alignSelf: 'center'
    },
    image1: {
        width: 60,
        height: 60,
        borderRadius: 4,
        marginTop: 270,
        marginHorizontal: 10,
        alignSelf: 'flex-end'
    },
})