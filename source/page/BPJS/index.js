import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, Dimensions, ScrollView, StatusBar } from 'react-native'

const { width } = Dimensions.get('window')
export default class index extends Component {
	render() {
		return (
			<View style={{ flex: 1 }}>
				<View>
					<StatusBar barStyle='dark-content' backgroundColor='white' />
					<Image style={styles.imageBanner} source={require('../../assets/Image/subhome.png')} />
					<View style={{ flexDirection: 'row', flexWrap: 'wrap', position: 'absolute', marginTop: 80, marginLeft: 20 }}>
						<Image style={{ width: 30, height: 30, backgroundColor: 'white', borderRadius: 5 }} source={require('../../assets/Icon/BPJS_Kesehatan.png')} />
						<Text style={{ fontSize: 15, fontWeight: 'bold', marginLeft: 30, marginTop: 5 }}>BPJS Kesehatan</Text>
					</View>
				</View>
				<ScrollView>
					<View style={{ flex: 1 }}>
						<StatusBar barStyle='dark-content' backgroundColor='white' />
						<View style={{ backgroundColor: 'white' }}>
							<Text style={styles.content}>Nomor BPJS</Text>
							<View style={styles.wrapper}>
								<Text style={{ marginLeft: 10, marginTop: 15, fontSize: 13, color: '#949494', marginBottom: 15 }}>Contoh 1234567890</Text>
							</View>
							<Text style={styles.contentPel}>Sampai Dengan</Text>
							<View style={styles.wrapperPel}>
								<Text style={{ marginLeft: 10, marginTop: 15, fontSize: 13, color: '#949494', marginBottom: 15 }}>Pilih Bulan</Text>
								<Image style={styles.image} source={require('../../assets/Icon/arrowDown.png')} />
							</View>
							<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 20, justifyContent: 'space-between' }}>
							</View>
						</View>
					</View>
				</ScrollView>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70, justifyContent: 'space-between' }}>
						<View style={styles.wrapperFooter}>
							<Text style={styles.textFooter}>Lanjut ke Pembayaran</Text>
						</View>
					</View>

			</View>
		)
	}
}
const styles = StyleSheet.create({
	imageBanner: {
		height: 120, width: 400,
	},
	header: {
		fontSize: 15,
		marginHorizontal: 30,
		color: '#949494',
		fontWeight: 'bold',
		top: 40
	},
	headerSub: {
		fontSize: 15,
		marginHorizontal: 30,
		color: '#4b239b',
		fontWeight: 'bold',
		top: 40
	},
	headerDivider: {
		height: 2,
		width: 175,
		backgroundColor: '#4b239b',
		marginLeft: 190,

	},
	content: {
		fontSize: 13,
		marginHorizontal: 16,
		color: '#949494',
		top: 20,
		backgroundColor: 'white'
	},
	contentPel: {
		fontSize: 13,
		marginHorizontal: 16,
		color: '#949494',
		top: 1,
		backgroundColor: 'white'
	},
	content2: {
		fontSize: 15,
		marginHorizontal: 13,
		color: 'black',
		fontWeight: 'bold',
		top: 10
	},
	wrapper: {
		backgroundColor: '#f0f0f0',
		elevation: 4,
		borderRadius: 4,
		width: width / -10 - 30,
		marginRight: 20,
		marginLeft: 20,
		marginBottom: 10,
		marginTop: 23,
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'space-between'
	},
	wrapperPel: {
		backgroundColor: '#f0f0f0',
		elevation: 4,
		borderRadius: 4,
		width: width / -10 - 30,
		marginRight: 20,
		marginLeft: 20,
		marginBottom: 10,
		marginTop: 10,
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'space-between'
	},
	image: {
		width: 20,
		height: 20,
		borderRadius: 4,
		marginTop: 15,
		marginHorizontal: 10,
	},
	image1: {
		width: 50,
		height: 50,
		borderRadius: 0,
		top: 30,
		marginHorizontal: 90,
	},

	divider: {
		width: width,
		height: 7,
		backgroundColor: '#ededed',
		marginVertical: 2
	},
	divider2: {
		width: width,
		height: 7,
		backgroundColor: '#ededed',
		marginVertical: 30
	},
	wrapperNominal: {
		backgroundColor: 'white',
		elevation: 3,
		borderRadius: 7,
		borderColor: '#e8e8e8',
		borderWidth: 1.5,
		width: 330,
		height: 120,
		marginRight: 5,
		marginLeft: 5,
		marginBottom: 0,
		marginTop: 15,
		flexDirection: 'row',
		flexWrap: 'wrap',

	},
	nominal: {
		fontSize: 13,
		color: 'black',
		fontWeight: 'bold',
		marginTop: 15,
		marginLeft: 15,
		marginRight: 10
	},
	nominalDetai: {
		fontSize: 13,
		marginLeft: 110,
		color: '#02bfbc',
		marginTop: 15,
		fontWeight: 'bold',
	},
	nominalSub: {
		fontSize: 12,
		marginLeft: 15,
		marginTop: 10,
		color: '#9e9999',
		top: 25
	},
	nominalRP: {
		fontSize: 13,
		marginLeft: 15,
		marginTop: 30,
		color: 'black',
		fontWeight: 'bold',
		top: 25
	},
	footer: {
		fontSize: 15,
		marginHorizontal: 30,
		color: 'white',
		fontWeight: 'bold',
		top: 65
	},
	wrapperFooter: {
		backgroundColor: 'white',
		elevation: 3,
		borderRadius: 20,
		backgroundColor: '#c9c9c9',
		width: 320,
		height: 45,
		marginRight: 10,
		marginLeft: 20,
		marginBottom: 20,
		marginTop: 13,
	},
	textFooter: {
		fontSize: 15,
		marginHorizontal: 85,
		color: 'white',
		fontWeight: 'bold',
		top: 13
	},



})