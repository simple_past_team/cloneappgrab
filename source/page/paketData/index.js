import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, Dimensions, ScrollView, StatusBar } from 'react-native'

const { width } = Dimensions.get('window')
export default class index extends Component {
	render() {
		return (
			<View style={{ flex: 1 }}>
				<View>
					<StatusBar barStyle='dark-content' backgroundColor='white' />
					<Image style={styles.imageBanner} source={require('../../assets/Image/subhome.png')} />
					<View style={{ flexDirection: 'row', flexWrap: 'wrap', position: 'absolute', marginTop: 80, marginLeft: 20 }}>
						<Image style={{ width: 30, height: 30, backgroundColor: 'white', borderRadius: 20 }} source={require('../../assets/Icon/logoTelkomsel.png')} />
						<Text style={{ fontSize: 15, fontWeight: 'bold', marginLeft: 30, marginTop: 5 }}>Telkomsel</Text>
					</View>
				</View>
				<ScrollView>
					<View style={{ flex: 1 }}>
						<StatusBar barStyle='dark-content' backgroundColor='white' />
						<View style={{backgroundColor: 'white'}}>
							<Text style={styles.content}>Nomor Ponsel</Text>
						<View style={styles.wrapper}>
							<Text style={{ marginLeft: 10, marginTop: 15, fontSize: 13, color: 'black', marginBottom: 15 }}>085956054957</Text>
							<Image style={styles.image} source={require('../../assets/Icon/cancel.png')} />
						</View>
						<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70, justifyContent: 'space-between'}}>
							<Text style={styles.header}>Isi Pulsa</Text>
							<Text style={styles.headerSub}>Paket Data</Text>
						</View>
						<View style={styles.headerDivider}></View>
						</View>
						<View style={{ flexWrap: 'wrap', flexDirection: 'row', marginLeft: 10, marginTop: 0, marginBottom: 20 }}>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Data 12.000 7 Hari</Text>
								<Text style={styles.nominalDetai}>Lihat Detail</Text>
								<Text style={styles.nominalSub}>Kuota 40MB - 110MB 7 Hari (sesuai zona terkait)</Text>
								<Text style={styles.nominalRP}>RP13.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Data 22.000 7 Hari</Text>
								<Text style={styles.nominalDetai}>Lihat Detail</Text>
								<Text style={styles.nominalSub}>Kuota 200MB - 420MB 7 Hari (sesuai zona terkait)</Text>
								<Text style={styles.nominalRP}>RP23.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Data 50.000 7 Hari</Text>
								<Text style={styles.nominalDetai}>Lihat Detail</Text>
								<Text style={styles.nominalSub}>Kuota 500MB - 600MB 7 Hari (sesuai zona terkait)</Text>
								<Text style={styles.nominalRP}>RP51.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Data 75.000 14 Hari</Text>
								<Text style={styles.nominalDetai}>Lihat Detail</Text>
								<Text style={styles.nominalSub}>Kuota 250MB - 510MB 14 Hari (sesuai zona terkait)</Text>
								<Text style={styles.nominalRP}>RP75.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Data 90.000 14 Hari</Text>
								<Text style={styles.nominalDetai}>Lihat Detail</Text>
								<Text style={styles.nominalSub}>Kuota 500MB - 600MB 14 Hari (sesuai zona terkait)</Text>
								<Text style={styles.nominalRP}>RP90.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Disney HotStar        </Text>
								<Text style={styles.nominalDetai}>Lihat Detail</Text>
								<Text style={styles.nominalSub}>Streaming film Disney dan berbagai film lainya</Text>
								<Text style={styles.nominalRP}>RP25.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>10 GB VIU 30 hari   </Text>
								<Text style={styles.nominalDetai}>Lihat Detail</Text>
								<Text style={styles.nominalSub}>Stream Drama Korea dan drama Asia Favorit lainya</Text>
								<Text style={styles.nominalRP}>RP30.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Kuota Game 30Hari</Text>
								<Text style={styles.nominalDetai}>Lihat Detail</Text>
								<Text style={styles.nominalSub}>Babas Main Game Favorit 1 bulan tanpa ribet</Text>
								<Text style={styles.nominalRP}>RP13.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Kuota Belajar 30GB</Text>
								<Text style={styles.nominalDetai}>Lihat Detail</Text>
								<Text style={styles.nominalSub}>Kuota khusus untuk pendidikan, masa aktif 1 Bulan</Text>
								<Text style={styles.nominalRP}>RP10</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Social Media 30GB</Text>
								<Text style={styles.nominalDetai}>Lihat Detail</Text>
								<Text style={styles.nominalSub}>Khusus untuk kamu yang ga mau ketinggalan Update</Text>
								<Text style={styles.nominalRP}>RP50.000</Text>
							</View>
						</View>
					</View>
				</ScrollView>
			</View>
		)
	}
}
const styles = StyleSheet.create({
	imageBanner: {
		height: 120, width: 400,
	},
	header: {
		fontSize: 15,
		marginHorizontal: 30,
		color: '#949494',
		fontWeight: 'bold',
		top: 40
	},
	headerSub: {
		fontSize: 15,
		marginHorizontal: 30,
		color: '#4b239b',
		fontWeight: 'bold',
		top: 40
	},
	headerDivider: {
		height: 2,
		width: 175,
		backgroundColor: '#4b239b',
		marginLeft: 190,

	},
	content: {
		fontSize: 13,
		marginHorizontal: 14,
		color: '#949494',
		top: 20,
		backgroundColor: 'white'
	},
	content2: {
		fontSize: 15,
		marginHorizontal: 13,
		color: 'black',
		fontWeight: 'bold',
		top: 10
	},
	wrapper: {
		backgroundColor: '#e7dded',
		elevation: 4,
		borderRadius: 4,
		width: width / -10 - 30,
		marginRight: 20,
		marginLeft: 20,
		marginBottom: 10,
		marginTop: 23,
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'space-between'
	},
	image: {
		width: 20,
		height: 20,
		borderRadius: 4,
		marginTop: 15,
		marginHorizontal: 10,
	},
	image1: {
		width: 50,
		height: 50,
		borderRadius: 0,
		top: 30,
		marginHorizontal: 90,
	},

	divider: {
		width: width,
		height: 7,
		backgroundColor: '#ededed',
		marginVertical: 2
	},
	divider2: {
		width: width,
		height: 7,
		backgroundColor: '#ededed',
		marginVertical: 30
	},
	wrapperNominal: {
		backgroundColor: 'white',
		elevation: 3,
		borderRadius: 7,
		borderColor: '#e8e8e8',
		borderWidth: 1.5,
		width: 330,
		height: 120,
		marginRight: 5,
		marginLeft: 5,
		marginBottom: 0,
		marginTop: 15,
		flexDirection: 'row',
		flexWrap: 'wrap',

	},
	nominal: {
		fontSize: 13,
		color: 'black',
		fontWeight: 'bold',
		marginTop: 15,
		marginLeft: 15,
		marginRight: 10
	},
	nominalDetai: {
		fontSize: 13,
		marginLeft: 110,
		color: '#02bfbc',
		marginTop: 15,
		fontWeight: 'bold',
	},
	nominalSub: {
		fontSize: 12,
		marginLeft: 15,
		marginTop: 10,
		color: '#9e9999',
		top: 25
	},
	nominalRP: {
		fontSize: 13,
		marginLeft: 15,
		marginTop: 30,
		color: 'black',
		fontWeight: 'bold',
		top: 25
	}


})