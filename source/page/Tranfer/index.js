import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, Dimensions, ScrollView, StatusBar } from 'react-native'

const { width } = Dimensions.get('window')
export default class index extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flexDirection: 'row', backgroundColor: '#4b239b', height: 100, justifyContent: 'space-between' }}>
                    <Text style={styles.header}>Penerima Baru</Text>
                    <Text style={styles.headerSub}>Favorit</Text>
                </View>
                <View style={styles.headerDivider}></View>
                <ScrollView>
                    <View style={{ flex: 1 }}>
                        <StatusBar barStyle='dark-content' backgroundColor='white' />
                    <View style={{ flex: 1, backgroundColor: '#f5f5f5' }}>
                        <View style={styles.wrapper}>
                            <Image style={styles.image} source={require('../../assets/Icon/transferIcon.png')} />
                            <Text style={{ marginTop: 18, marginLeft: -100, fontSize: 15, color: 'black' }}>Ke Semua OVO</Text>
                            <Image style={styles.arrow} source={require('../../assets/Icon/arrowRight.png')} />
                        </View>
                        <View style={styles.wrapper2}>
                            <Image style={styles.image} source={require('../../assets/Icon/bank.png')} />
                            <Text style={{ marginTop: 18, marginLeft: -77, fontSize: 15, color: 'black' }}>Ke Rekening Bank</Text>
                            <Image style={styles.arrow} source={require('../../assets/Icon/arrowRight.png')} />
                        </View>
                        </View>
                        <View style={styles.divider}></View>
                        <View style={{ marginBottom: 10 }}>
                            <Text style={styles.content2}>Transaksi Terakhir</Text>
                        </View>
                        <View>
                        <Image style={{width: 50, height: 50, borderRadius: 100, marginLeft: 15, marginTop: 5}} source={require('../../assets/Icon/transHistory.png')} />
                        </View>
                        <View style={styles.divider1}></View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    header: {
        fontSize: 15,
        marginHorizontal: 30,
        color: 'white',
        fontWeight: 'bold',
        top: 65
    },
    headerSub: {
        fontSize: 15,
        marginHorizontal: 50,
        color: '#b097c9',
        fontWeight: 'bold',
        top: 65
    },
    headerDivider: {
        height: 3,
        width: 190,
        backgroundColor: '#02bfbc',
        marginVertical: 0,

    },
    content2: {
        fontSize: 15,
        marginHorizontal: 13,
        color: 'black',
        fontWeight: 'bold',
        top: 10
    },
    wrapper: {
        backgroundColor: 'white',
        elevation: 4,
        borderRadius: 4,
        width: 320,
        height: 60,
        marginRight: 20,
        marginLeft: 20,
        marginBottom: 1,
        marginTop: 30,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    },
    wrapper2: {
        backgroundColor: 'white',
        elevation: 4,
        borderRadius: 4,
        width: 320,
        height: 60,
        marginRight: 20,
        marginLeft: 20,
        marginBottom: 20,
        marginTop: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    },
    image: {
        width: 40,
        height: 40,
        borderRadius: 4,
        marginTop: 8,
        marginHorizontal: 10,
    },
    arrow: {
        width: 20,
        height: 20,
        borderRadius: 4,
        marginTop: 18,
       marginHorizontal: 15
    },
    divider: {
        width: width,
        height: 7,
        backgroundColor: '#ededed',
        marginVertical: 2
    },
    divider1: {
        width: width,
        height: 3,
        backgroundColor: '#ededed',
        marginVertical: 5
    }



})