/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { View, Text, Image, Dimensions, StyleSheet, StatusBar, ScrollView } from 'react-native';
import OvoComponent from '../../component/OvoComponent';
import MainComponent from '../../component/MainComponent';
import PromoItems from '../../component/PromoItems';
import InfoPromo from '../../component/InfoPromo'

const { height, width } = Dimensions.get('window')

class Home extends React.Component {
  render() {
    return (

      <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#4b239b', height:60 }}>
              <Text style={styles.header}>OVO</Text>
              <Image style={styles.notif} source={require('../../assets/Icon/niotif.png')} />
        </View>
        <ScrollView>
          <View style={{ flex: 1 }}>
            <StatusBar barStyle='dark-content' backgroundColor='white' />
            <Image style={styles.imageBanner} source={require('../../assets/Image/home.png')} />
            <Text style={styles.greetingText}>OVO Cash</Text>
            <Text style={styles.OVO}>Rp</Text>
            <Text style={styles.saldo}>1.000.000</Text>
            <Text style={styles.ovopoin}>OVO Points</Text>
            <Text style={styles.poin}>250</Text>
            <View style={styles.wrapperOvo}>
              <OvoComponent />

            </View>
            <View style={{ marginHorizontal: 18 }}>
              <MainComponent />
            </View>
            <View style={styles.divider}></View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={styles.info}>Info dan Promo Spesial</Text>
              <Text style={styles.lihat}>Lihat Semua</Text>
            </View>

            <InfoPromo />
            <View style={styles.divider}></View>
            <View style={{ justifyContent: 'space-between' }}>
              <Text style={styles.info}>Yang Menarik di OVO</Text>
              <Text style={styles.infosub}>Jangan ngaku update kalau belum cobain fitur ini</Text>
            </View>
            <PromoItems />
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  imageBanner: {
    height: 200, width: width,
    borderBottomRightRadius: 30,
    borderBottomLeftRadius: 30
  },
  greetingText: {
    fontSize: 15,
    position: 'absolute',
    marginLeft: 10,
    top: 20,
    color: 'white'
  },
  OVO: {
    fontSize: 15,
    position: 'absolute',
    marginLeft: 10,
    top: 55,
    color: 'white'
  },
  saldo: {
    fontSize: 23,
    position: 'absolute',
    marginLeft: 30,
    top: 58,
    color: 'white'
  },
  ovopoin: {
    fontSize: 15,
    position: 'absolute',
    marginLeft: 10,
    top: 100,
    color: 'white'
  },
  poin: {
    fontSize: 15,
    position: 'absolute',
    marginLeft: 90,
    top: 100,
    color: '#f2c200'
  },
  wrapperOvo: {
    marginHorizontal: 18,
    height: 85,
    marginTop: -50,
    backgroundColor: 'white',
    elevation: 4,
    borderRadius: 10,
  },
  textOVO: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 12,
    marginTop: 10,
  },
  garisDiOvo: {
    height: .8,
    backgroundColor: '#adadad',
    marginTop: 10,
    marginHorizontal: 5,
  },
  divider: {
    width: width,
    height: 10,
    backgroundColor: '#ededed',
    marginVertical: 15
  },
  info: {
    fontSize: 18,
    marginHorizontal: 15,
    top: 5,
    color: 'black',
    fontWeight: 'bold'
  },
  infosub: {
    fontSize: 12,
    marginHorizontal: 15,
    top: 5,
    color: '#6f7575',

  },
  lihat: {
    fontSize: 15,
    marginHorizontal: 15,
    top: 5,
    color: '#02bfbc',
  },
  
  header: {
    fontSize: 28,
    marginHorizontal: 10,
    color: 'white',
    fontWeight: 'bold',
    top: 12
  },
  notif: {
    width:30,
    height:30,
    top: 12,
    marginHorizontal: 3,
  }
})
export default Home