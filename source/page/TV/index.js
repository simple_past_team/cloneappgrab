import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, ScrollView } from 'react-native'

export default class index extends Component {
	render() {
		return (
			<ScrollView>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70 }}>
					<Image style={styles.image} source={require('../../assets/Icon/biznet.png')} />
					<Text style={styles.plntext}>Biznet Home</Text>
					<Image style={styles.arrow} source={require('../../assets/Icon/arrowRight.png')} />
				</View>
				<View style={styles.divider}></View>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70 }}>
					<Image style={styles.image} source={require('../../assets/Icon/CBN.png')} />
					<Text style={styles.plntext}>CBN</Text>
					<Image style={styles.arrow2} source={require('../../assets/Icon/arrowRight.png')} />
				</View>
				<View style={styles.divider}></View>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70 }}>
					<Image style={styles.image} source={require('../../assets/Icon/first.png')} />
					<Text style={styles.plntext}>First Media</Text>
					<Image style={styles.arrow3} source={require('../../assets/Icon/arrowRight.png')} />
				</View>
				<View style={styles.divider}></View>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70 }}>
					<Image style={styles.image} source={require('../../assets/Icon/indihome.png')} />
					<Text style={styles.plntext}>Indi Home</Text>
					<Image style={styles.arrow4} source={require('../../assets/Icon/arrowRight.png')} />
				</View>
				<View style={styles.divider}></View>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70 }}>
					<Image style={styles.image} source={require('../../assets/Icon/KVis.png')} />
					<Text style={styles.plntext}>K Vision</Text>
					<Image style={styles.arrow5} source={require('../../assets/Icon/arrowRight.png')} />
				</View>
				<View style={styles.divider}></View>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70 }}>
					<Image style={styles.image} source={require('../../assets/Icon/mnc.png')} />
					<Text style={styles.plntext}>MNC Play</Text>
					<Image style={styles.arrow4} source={require('../../assets/Icon/arrowRight.png')} />
				</View>
				<View style={styles.divider}></View>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70 }}>
					<Image style={styles.image} source={require('../../assets/Icon/mnc.png')} />
					<Text style={styles.plntext}>MNC Vision</Text>
					<Image style={styles.arrow6} source={require('../../assets/Icon/arrowRight.png')} />
				</View>
				<View style={styles.divider}></View>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70 }}>
					<Image style={styles.image} source={require('../../assets/Icon/myrepublic.png')} />
					<Text style={styles.plntext}>My Republic</Text>
					<Image style={styles.arrow6} source={require('../../assets/Icon/arrowRight.png')} />
				</View>
				<View style={styles.divider}></View>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70 }}>
					<Image style={styles.image} source={require('../../assets/Icon/transMed.png')} />
					<Text style={styles.plntext}>Trans vision</Text>
					<Image style={styles.arrow6} source={require('../../assets/Icon/arrowRight.png')} />
				</View>
				<View style={styles.divider}></View>
				<View style={{ height: 80}}></View>
			</ScrollView>
		)
	}
}
const styles = StyleSheet.create({
	image: {
		width: 40,
		height: 40,
		borderRadius: 25,
		top: 10,
		marginHorizontal: 10,
	},
	plntext: {
		fontSize: 15,
		color: 'black',
		fontWeight: 'bold',
		marginHorizontal: 10,
		top: 18
	},
	arrow: {
		width: 20,
		height: 20,
		borderRadius: 25,
		top: 20,
		marginHorizontal: 153,
	},
	arrow2: {
		width: 20,
		height: 20,
		borderRadius: 25,
		top: 20,
		marginHorizontal: 210,
	},
	arrow3: {
		width: 20,
		height: 20,
		borderRadius: 25,
		top: 20,
		marginHorizontal: 165,
	},
	arrow4: {
		width: 20,
		height: 20,
		borderRadius: 25,
		top: 20,
		marginHorizontal: 172,
	},
	arrow5: {
		width: 20,
		height: 20,
		borderRadius: 25,
		top: 20,
		marginHorizontal: 184,
	},
	arrow6: {
		width: 20,
		height: 20,
		borderRadius: 25,
		top: 20,
		marginHorizontal: 163,
	},

	divider: {
		height: 0.1,
		backgroundColor: '#ededed',
		marginVertical: 1
	}
})