import React, { Component } from 'react'
import { Text, View, Image, StyleSheet } from 'react-native'

export default class index extends Component {
	render() {
		return (
			<View>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 60 }}>
					<Image style={styles.pln} source={require('../../assets/Icon/perusahaan_listrik_negara.png')} />
					<Text style={styles.plntext}>Token Listrik</Text>
					<Image style={styles.arrow} source={require('../../assets/Icon/arrowRight.png')} />
				</View>
				<View style={styles.divider}></View>
				<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 60 }}>
					<Image style={styles.pln} source={require('../../assets/Icon/perusahaan_listrik_negara.png')} />
					<Text style={styles.plntext}>Tagihan Listrik</Text>
					<Image style={styles.arrow2} source={require('../../assets/Icon/arrowRight.png')} />
				</View>
			</View>
		)
	}
}
const styles = StyleSheet.create({
	pln: {
		width: 40,
		height: 40,
		borderRadius: 25,
		top: 10,
		marginHorizontal: 10,
	},
	plntext: {
		fontSize: 15,
		color: 'black',
		fontWeight: 'bold',
		marginHorizontal: 10,
		top: 18
	},
	arrow: {
		width: 35,
		height: 35,
		borderRadius: 25,
		top: 12,
		marginHorizontal: 150,
	},
	arrow2: {
		width: 35,
		height: 35,
		borderRadius: 25,
		top: 12,
		marginHorizontal: 137,
	},
	divider: {
		height: 2,
		backgroundColor: '#ededed',
		marginVertical: 1
	}
})