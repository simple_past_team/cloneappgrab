import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, Dimensions, ScrollView, StatusBar } from 'react-native'

const { width } = Dimensions.get('window')
export default class index extends Component {
	render() {
		return (
			<View style={{ flex: 1 }}>
				<View>
					<StatusBar barStyle='dark-content' backgroundColor='white' />
					<Image style={styles.imageBanner} source={require('../../assets/Image/subhome.png')} />
					<View style={{ flexDirection: 'row', flexWrap: 'wrap', position: 'absolute', marginTop: 80, marginLeft: 20 }}>
						<Image style={{ width: 30, height: 30, backgroundColor: 'white', borderRadius: 20 }} source={require('../../assets/Icon/logoTelkomsel.png')} />
						<Text style={{ fontSize: 15, fontWeight: 'bold', marginLeft: 30, marginTop: 5 }}>Telkomsel</Text>
					</View>
				</View>
				<ScrollView>
					<View style={{ flex: 1 }}>
						<StatusBar barStyle='dark-content' backgroundColor='white' />
						<View style={{backgroundColor: 'white'}}>
							<Text style={styles.content}>Nomor Ponsel</Text>
						<View style={styles.wrapper}>
							<Text style={{ marginLeft: 10, marginTop: 15, fontSize: 13, color: 'black', marginBottom: 15 }}>085956054957</Text>
							<Image style={styles.image} source={require('../../assets/Icon/cancel.png')} />
						</View>
						<View style={{ flexDirection: 'row', backgroundColor: 'white', height: 70, justifyContent: 'space-between'}}>
							<Text style={styles.header}>Isi Pulsa</Text>
							<Text style={styles.headerSub}>Paket Data</Text>
						</View>
						<View style={styles.headerDivider}></View>
						</View>
						<View style={{ flexWrap: 'wrap', flexDirection: 'row', marginLeft: 10, marginTop: 0, marginBottom: 20 }}>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp5.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp10.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp15.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp20.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp25.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp30.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp40.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp50.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp75.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp100.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp150.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp200.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp300.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp500.000</Text>
							</View>
							<View style={styles.wrapperNominal}>
								<Text style={styles.nominal}>Rp1.000.000</Text>
							</View>
						</View>
					</View>
				</ScrollView>
			</View>
		)
	}
}
const styles = StyleSheet.create({
	imageBanner: {
		height: 120, width: 400,
	},
	header: {
		fontSize: 15,
		marginHorizontal: 30,
		color: '#4b239b',
		fontWeight: 'bold',
		top: 40
	},
	headerSub: {
		fontSize: 15,
		marginHorizontal: 30,
		color: '#949494',
		fontWeight: 'bold',
		top: 40
	},
	headerDivider: {
		height: 2,
		width: 175,
		backgroundColor: '#4b239b',
		marginVertical: 0,

	},
	content: {
		fontSize: 13,
		marginHorizontal: 14,
		color: '#949494',
		top: 20,
		backgroundColor: 'white'
	},
	content2: {
		fontSize: 15,
		marginHorizontal: 13,
		color: 'black',
		fontWeight: 'bold',
		top: 10
	},
	wrapper: {
		backgroundColor: '#e7dded',
		elevation: 4,
		borderRadius: 4,
		width: width / -10 - 30,
		marginRight: 20,
		marginLeft: 20,
		marginBottom: 10,
		marginTop: 23,
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'space-between'
	},
	image: {
		width: 20,
		height: 20,
		borderRadius: 4,
		marginTop: 15,
		marginHorizontal: 10,
	},
	image1: {
		width: 50,
		height: 50,
		borderRadius: 0,
		top: 30,
		marginHorizontal: 90,
	},

	divider: {
		width: width,
		height: 7,
		backgroundColor: '#ededed',
		marginVertical: 2
	},
	divider2: {
		width: width,
		height: 7,
		backgroundColor: '#ededed',
		marginVertical: 30
	},
	wrapperNominal: {
		backgroundColor: 'white',
		elevation: 3,
		borderRadius: 7,
		borderColor: '#e8e8e8',
		borderWidth: 1.5,
		width: 150,
		height: 70,
		marginRight: 10,
		marginLeft: 10,
		marginBottom: 0,
		marginTop: 15,
	},
	nominal: {
		fontSize: 13,
		alignSelf: 'center',
		color: 'black',
		fontWeight: 'bold',
		marginTop: 23
	},
	content3: {
		fontSize: 12,
		marginHorizontal: 13,
		color: '#9e9999',
		top: 10,
	},
	wrapperNominal2: {
		backgroundColor: 'white',
		elevation: 3,
		borderRadius: 5,
		backgroundColor: '#e8e8e8',
		width: 330,
		height: 45,
		marginRight: 10,
		marginLeft: 5,
		marginBottom: 5,
		marginTop: 0,
	},
	nominal2: {
		fontSize: 13,
		marginHorizontal: 20,
		color: '#9e9999',
		top: 13
	},
	wrapperkartu: {
		backgroundColor: 'white',
		elevation: 3,
		borderRadius: 5,
		backgroundColor: '#e8e8e8',
		borderWidth: 1,
		borderColor: '#02bfbc',
		width: 220,
		height: 130,
		marginRight: 10,
		marginLeft: 5,
		marginBottom: 20,
		marginTop: 2,
	},
	nominalkartu: {
		fontSize: 14,
		marginHorizontal: 20,
		color: '#9e9999',
		top: 25
	}

})